# โปรดอ่าน

สำหรับทดสอบ บริษัท 7 idea.\

Code ชุดนี้ตอน Develop ใช้ Docker ในการ Coding.\

`/si-test-frontend` ถูกสร้างขึ้นโดย `create-react-app`

### จึงไม่ได้ทดสอบโดยใช้ node version อื่นนอกจาก Node 14.18.2

---

## วิธี Run ด้วย Docker-Compose

- ติดตั้ง Docker
- มาที่ root สั่งคำสั่ง `docker-compose up`
- ทดสอบได้ที่ `localhost:3000`

## วิธี Run ด้วย npm

- ติดตั้ง nodejs version 14.18.2
- เข้าไปที่ `/si-test-frontend`
- สั่งคำสั่ง `npm install`
- สั่งคำสั่ง `npm start`
- ทดสอบได้ที่ `localhost:3000`

---
