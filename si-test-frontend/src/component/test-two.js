import React, {useEffect, useState} from "react";

export default function TestTwo() {

    const [ansEasy, setAnsEasy] = useState('')
    const [ansHard, setAnsHard] = useState('')

    const getData = (key) => {
        fetch(`data/${key}.json`)
            .then((response) => {
                return response.json();
            })
            .then((myJson) => {
                if (key === 'easy') {
                    setAnsEasy(calMaxNumberByArray(myJson));
                } else {
                    setAnsHard(calMaxNumberByArray(myJson));
                }
            });
    }

    useEffect(() => {
        getData('easy');
        getData('hard');
    }, []);

    const calMaxNumberByArray = (values) => {
        let result = 0;
        values.forEach((val) => {
            result += Math.max(...val);
        })
        return result;
    }

    return (<div>
        <h2>Ans easy: {ansEasy}</h2>
        <h2>Ans hard: {ansHard}</h2>
    </div>)
}
