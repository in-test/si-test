import React, {useState} from "react";

export default function TestOne() {

    const [ansArr, setAnsArr] = useState([]);
    const [inputText, setInputText] = useState('');


    const onSubmit = () => {
        if (inputText !== '') {
            let result = ansArr;
            result.push(inputText);
            setInputText('');
            setAnsArr(result)
            setTimeout(() => {
                ansArr.shift()
                setAnsArr([...ansArr]);
            }, 5000)
        }
    };

    const handleChange = (event) => {
        setInputText(event.target.value);
    }

    return (
        <div>
            <label>
                <input type="text" value={inputText} onChange={handleChange}/>
            </label>
            <input type="submit" value="Submit" onClick={onSubmit}/>
            {ansArr && ansArr.map((item, index) => (
                <IntervalText intervalText={item} key={`int-${index}`}/>))}
        </div>
    );
}

function IntervalText({intervalText}) {
    return (<h1>{intervalText}</h1>)
}
