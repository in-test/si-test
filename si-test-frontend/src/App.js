import './App.css';
import TestOne from './component/test-one';
import TestTwo from "./component/test-two";

function App() {
    return (<div className="App">
        <table>
            <tbody>
            <tr>
                <td>
                    <TestOne/>
                </td>
                <td>
                    <TestTwo/>
                </td>
            </tr>
            </tbody>
        </table>
    </div>);
}

export default App;
